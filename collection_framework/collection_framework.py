from collections import Counter
from functools import cache
import argparse


@cache
def num_uniq_char(string: str) -> int:
    if not isinstance(string, str):
        raise TypeError(f'Input must be text!')
    number_uniq_char = len(
        {key: val for key, val in Counter(string).items() if key != ' ' and key != '\n' and val == 1})
    return number_uniq_char


def num_uniq_char_file(file):
    with open(file, 'r') as sourceFile:
        number_uniq_char_file = num_uniq_char(sourceFile.read())
    return number_uniq_char_file


def parser_cli():
    parser = argparse.ArgumentParser(description='The num of unique char in the strings')
    parser.add_argument('-s', '--string', type=str, required=False, help='Return the num of unique char in the string')
    parser.add_argument('-f', '--file', required=False, help='Return the num of unique char in the text file')
    args = parser.parse_args()

    if args.file:
        return num_uniq_char_file(args.file)
    elif args.string:
        return num_uniq_char(args.string)


if __name__ == '__main__':
    print(parser_cli())
