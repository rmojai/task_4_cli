import argparse
import pytest
from unittest.mock import patch, mock_open
from collection_framework.collection_framework import num_uniq_char_file, parser_cli


@patch('builtins.open', mock_open(read_data="qwerty"))
def test_open_file():
    assert num_uniq_char_file('') == 6


@patch('argparse.ArgumentParser.parse_args', return_value=argparse.Namespace(string='qwerty', file=None))
def test_parser_cli_string(mock_args):
    assert parser_cli() == 6


@patch('argparse.ArgumentParser.parse_args', return_value=argparse.Namespace(string='qwerty', file='file'))
@patch('builtins.open', mock_open(read_data="qwerty12345"))
def test_cli_pars_file(mock_args):
    assert parser_cli() == 11


if __name__ == '__main__':
    pytest.main()
